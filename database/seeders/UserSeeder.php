<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'system',
            'username' => 'admin',
            'phone' => '01000000000',
            'email' => 'admin@admin.com',
            'password' => 'password',
        ]);
    }
}
