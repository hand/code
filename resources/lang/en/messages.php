<?php

return [
    'field_process' => 'We were unable to process your request, please try again later',
    'cannot_deleted_your_account' => 'Sorry, your account cannot be deleted',
    'there_are_no_data' => 'There are no data',
    'have_some_errors' => 'You have some errors, please re-check the added data',
];
