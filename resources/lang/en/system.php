<?php

return [

    /* LOGIN & AUTHENTICATIONS */

    'welcome-login-page'            =>  'تسجيل الدخول لحسـابك ',
    'email_or_username'             =>  'البريد الإلكتروني أو إسم المستخدم',
    'password'                      =>  'Password',
    'remember_me'                   =>  'تذكرني',
    'logout'                        =>  'Logout',
    'btn-login'                     =>  'تسجيل الدخـول',
    'forgot-password'               =>  'نسيت كلمة المرور ؟',
    'Login-failed'                  =>  'عفوًا .. خطـأ في :input أو كلمة المرور! أو ربما تم تعطيل حسابك.',
    'forget-password-failed'        =>  'عفوًا ، هذا البريد غير موجود بالنظام , يرجى كتابة البريد الخاص بك لإسترداد حسابك .',
    'forget-password-no-worry'      =>  'لا تقلق ، إضغط هنا لإعادة تعيين كلمة المرور الخاصة بك .',
    'recover-password-title'        =>  'إستعادة كلمة السر الخاصة بك',
    'recover-password-note'         =>  'أدخل عنوان بريدك الإلكتروني أدناه لإعادة تعيين كلمة المرور الخاصة بك.',
    'recover-password-btn'          =>  'إعادة تعيين كلمة المرور.',
    'know-your-password'            =>  'تعرف كلمة المرور الخاصة بك ؟',
    'recover-password-success'      =>  'اذا كان هذا البريد موجود بالنظام سيتم إرسال بريد إلكتروني به رابط لإعادة تعيين كلمة المرور الخاصة بك. إذا لم يظهر في غضون بضع دقائق ، فتحقق من مجلد الرسائل غير المرغوب فيها.',
    'reset-password-title'          =>  'قم بتعيين كلمة مرور جديدة.',
    'reset-password-btn'            =>  'تغيير كلمة السر',
    'new-password'                  =>  'كلمة السر الجديدة',
    'confirm-new-password'          =>  'تأكيد كلمة المرور الجديدة',
    'new-password-saved'            =>  'تم حفظ كلمة المرور الجديدة بنجاح !',
    'retry-reset-password'          =>  'إسترجاع كلمه المرور مره أخري!',
    'reset-password-token-expired'  =>  'عفوًا .. صلاحية هذا الرابط منتهيه .',
    'recover-password-content-mail' =>  'We heard that you lost your account password. Sorry about that! But don’t worry! You can use the following link to reset your password',

    /* LOGIN & AUTHENTICATIONS */

    /* SYSTEM GENERAL  */

    'system-name'                   =>  'System Dashboard v1.0',
    'back'                          =>  'رجـوع',
    'go-to-home'                    =>  'ذهاب للرئيسيه',

    'id'                            =>  'ID#',
    'name'                          =>  'Name',
    'phone'                         =>  'Phone',
    'first_name'                    =>  'First Name',
    'last_name'                     =>  'Last Name',
    'username'                      =>  'Username',
    'is_active'                     =>  'حاله التفعيل',
    'created_at'                    =>  'Created At',
    'updated_at'                    =>  'Updated At',
    'control'                       =>  ' خيارات',
    'active'                        =>  'مفعل',
    'inactive'                      =>  'غير مفعل',
    'email'                         =>  'Email',
    'trigger_tools'                 =>  'Tools',
    'add_new'                       =>  'أضف جديد',
    'add-new'                       =>  'أضف جديد',
    'edit'                          =>  'Edit',
    'show'                          =>  'عرض',
    'delete'                        =>  'Delete',
    'submit'                        =>  'حفظ',
    'cancel'                        =>  'Cancel',
    'leave_blank'                   =>  'إتركها فارغه حتي لا تتغير !',
    'current_image'                 =>  'الصوره الحاليه',
    'attach_image'                  =>  'إرفاق صورة',
    'remove'                        =>  'إزاله',
    'change'                        =>  'تغيير',
    'select_image'                  =>  'تحديد صوره',
    'select'                        =>  'اختار..',
    'save'                          =>  'حفظ',
    'home-page'                     =>  'الصفحة الرئيسية',
    'status'                        =>  'الحالة',
    'tools'                         => 'Tools',
    'by'                            => 'By',
    'close'                         => 'Close',
    'created'                       =>  'Added successfully.',
    'create-new'                    =>  'Create new',
    'updated'                       =>  ' Modifications saved successfully. ',
    'deleted'                       =>  ' Deleted successfully. ',
    'error'                         =>  ' عفوًا , حدث خطأ ما !',
    'other'                         =>  ' تمت العملية بنجاح .',
    'can_not_delete_super_admin'    =>  ' عفوًا .. لا يمكنك حذف المدير الرئيسي للنظام  ',
    'click-here'                    =>  'Click Here',
    'no-data-to-show'               =>  'عفوًا لا يوجد بيانات للعرض !',
    'active-inactive'               =>  'مفعل / غير مفعل ',
    'not-authorized'                =>  'عفوًا .. لا يوجد لديك صلاحيه لتنفيذ هذا الطلب ',
    'dashboard'                     =>  'Dashboard',
    'users'                         =>  'Users',
    'all-users'                     =>  'All users',
    'create-user'                   =>  'Create user',
    'edit-user'                     =>  'Edit user',
    'user-information'              =>  'User information',
    'sliders'                       =>  'Sliders',
    'slider'                        =>  'Slider',
    'all-sliders'                   =>  'All Sliders',
    'create-slider'                 =>  'create slider',
    'edit-slider'                   =>  'Edit slider',
    'slider-details'                =>  'slider details',
    'image'                         =>  'Image',
    'videos'                        =>  'Videos',
    'all-videos'                    =>  'All Videos',
    'video-details'                 =>  'Video details',
    'create-video'                  =>  'Create Video',
    'edit-video'                    =>  'Edit Video',
    'galleries'                     =>  'Galleries',
    'gallery-group'                 =>  'Gallery Group',
    'create-gallery-group'          =>  'Create Gallery Group',
    'gallery-group-details'         =>  'Gallery group details',
    'create-gallery'                =>  'Create Gallery',
    'edit-gallery'                  =>  'Edit Gallery',
    'images'                        =>  'Images',
    'create-image'                  =>  'Create Image',
    'edit-image'                    =>  'Edit Image',
    'contactus'                     =>  'Contactus',
    'sponsors'                      =>  'Sponsors',
    'sponsor'                       =>  'Sponsor',
    'sponsor-details'               =>  'Sponsor details',
    'create-sponsor'                =>  'Create sponsor',
    'edit-sponsor'                  =>  'Edit sponsor',
    'articles'                      =>  'Articles',
    'article'                       =>  'Article',
    'article-details'               =>  'Article details',
    'create-article'                =>  'Create article',
    'edit-article'                  =>  'Edit article',
    'organizations'                 =>  'Organizations',
    'organization'                  =>  'Organization',
    'organization-details'          =>  'Organization details',
    'create-organization'           =>  'Create organization',
    'edit-organization'             =>  'Edit organization',
    'password-data'                 =>  'Password data',
    'password-confirmation'         =>  'Password confirmation',
    'add-then-close'                =>  'Add then close',
    'add-then-add-new'              =>  'Add then add new',
    'title'                         =>  'Title',
    'sub-title'                     =>  'Sub title',
    'link'                          =>  'Link',
    'date'                          =>  'Date',
    'seen'                          =>  'Seen',
    'message-details'               =>  'Message details',
    'details'                       =>  'details',
    'message'                       =>  'Message',
    'new'                           =>  'New',
    'description'                   =>  'Description',
    'body'                          =>  'body',
    'employee_name'                 =>  'Employee name',
    'employee_phone'                =>  'Employee phone',
    'employee_position'             =>  'Employee position',
    'organization_name'             =>  'Organization Name',
    'account-information'           =>  'Account information',
    'employee-information'          =>  'Employee information',
    'pages'                         =>  'Pages',
    'edit-page'                     =>  'Edit Page',
    'setting'                       =>  'Setting',
    'all-admins'                    =>  'All Admins',
    'competitions'                  =>  'Competitions',
    'create-competition'            =>  'Create Competition',
    'edit-competition'              =>  'Edit Competition',
    'competition-details'           =>  'Competition Details',
    'competition-terms'             =>  'Competition Terms',
    'competition-tables'            =>  'Competition Tables',
    'competition'                   =>  'Competition',
    'files'                         =>  'Files',
    'file'                          =>  'File',
    'competition-files'             =>  'Competition Files',
    'matches'                       =>  'Matches',
    'competition-matches'           =>  'Competition Matches',
    'first-team'                    =>  'First Team',
    'second-team'                   =>  'Second Team',
    'stadium_name'                  =>  'Stadium Name',
    'location'                      =>  'Location',
    'first-organization-score'      =>  'First Organization Score',
    'second-organization-score'     =>  'Second Organization Score',
    'day'                           =>  'Day',
    'match-date'                    =>  'Match Date',
    'match-time'                    =>  'Match Time',
    'stage'                         =>  'Stage',
    'job-title'                     =>  'Job Title',
    'create-board'                  =>  'Create Board',
    'directors'                     =>  'Directors Board',
    'director'                      =>  'Director Board',
    'crate-director'                =>  'Crate Director Board',
    'edit-director'                 =>  'Edit Director Board',
    'members'                       =>  'Members',
    'member'                        =>  'member',
    'create-member'                 =>  'Crate Member',
    'edit-member'                   =>  'Edit Member',
    'type'                          =>  'type',
    'video-link'                    =>  'Video link',
    /* SYSTEM GENERAL  */


    /* ADMINS MODULE */

    'admins-index-title'            =>  'إداره مديري النظام',
    'admin-group'                   =>  'المجموعه الإداريه',
    'can-sort-sidebar'              =>  'ترتيب القائمه الجانبيه ؟',
    'admin-edit'                    =>  'تعديل بيانات المدير',
    //'account-was-disabled'          => 'Sorry, You Account has been disabled. Please Contact with Super Admin',
    'account-was-disabled'          =>  'عفوًا .. لقد تم تعطيل حسابك يرجي التواصل مع المدير الرئيسي للنظام , شكرا لك. ',

    /* ADMINS MODULE */

    /* PERMISSIONS MODULE */

    'add-new-controller-for-permissions' => 'إضافه مديول جديد لصلاحيات النظام',
    'select-your-controller' => 'يرجي تحديد المديول الجديد',
    'display-in-group-permissions-as' => 'عرض في المجموعات الإداريه بإسم',
    'controller-name' => 'إسم الكنترولر ',
    'assign-permissions-now' => 'تحديد الصلاحيات الآن',
    'assign-actions-to-controller' => ' إضافه صلاحيات لهذا الكنترولر ',
    'all-methods-available-in-this-controller' => 'جميع الوظائف المتاحه لهذا الكنترولر ',
    'function-name' => 'الصلاحيه',
    'action-method-name' => 'الوظيفه',
    'has-all-permissions' => 'إضافه كل الصلاحيات تلقائياً',

    /* PERMISSIONS MODULE */

    /* Groups MODULE */

    'groups' =>[
        'module-name'=>'المجموعات الإدارية',
        'create' => 'إضافه مجموعه جديده',
        'edit' => 'تعديل مجموعه إداريه',
        'name' => 'اسم المجموعه',
        'permissions' => 'صلاحيات المجموعه',
        'not-defined' => 'لم تحدد بعد',
        'can-not-delete-group' => 'عفوًا .. لايمكنك حذف المجموعه برجاء حذف جميع المديرين المرتبطين بهذه المجموعه أولاً.',
    ],

    /* Groups MODULE */

    /* SWEET ALERT */
    'msgForDelete'  => 'Are you sure you want to delete!',
    'msgForRenew'  => 'هل تريد تجديد هذا الاشتراك',
    'msgForSuspend'  => 'هل تريد ايقاف هذا الخط',
    'le'    => 'جـنية',
    'total' => 'الاجمالي',
    'print' => 'طباعة',
];

