@extends('frontend.layouts.master')

@section('title', 'article')

@section('content')
    <div id="heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-content">
                        <h2>Single Product</h2>
                        <span><a href="{{ url('/') }}">Articles</a> / Single Post </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="product-post">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-section">
                        <h2>Single Blog Post</h2>
                        <img src="{{ asset('assets/frontend/images/under-heading.png') }}" alt="" />
                    </div>
                </div>
            </div>

            <div id="single-blog" class="page-section first-section">
                <div class="container">

                    <div class="row">
                        <div class="product-item col-md-12">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="image">
                                        <div class="image-post">
                                            <img src="{{ $article->articleImage }}" alt="">
                                        </div>
                                    </div>

                                    <div class="product-content">
                                        <div class="product-title">
                                            <h3>{{ $article->title }}</h3>
                                            <span class="subtitle">{{ $article->comments->count() }} comments</span>
                                        </div>
                                        <p>{!! $article->body !!}</p>
                                    </div>

                                    <div class="divide-line">
                                        <img src="{{ asset('assets/frontend/images/div-line.png') }}" alt="" />
                                    </div>
                                    <div class="comments-title">
                                        <div class="comment-section">
                                            <h4>{{ $article->comments->count() }} comments</h4>
                                        </div>
                                    </div>

                                    <div class="all-comments">
                                        @foreach($article->comments as $comment)
                                            <div class="view-comments">
                                                <div class="comments">
                                                    <div class="comment-body">
                                                        <h6>{{ $comment->visitor->name }}</h6>
                                                        <span class="date">{{ $comment->created_at->diffForHumans() }}</span>
                                                        <p>{{ $comment->comment }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="divide-line">
                                        <img src="{{ asset('assets/frontend/images/div-line.png') }}" alt="" />
                                    </div>

                                    <div class="leave-comment">
                                        <div class="leave-one">
                                            <h4>Leave a comment</h4>
                                        </div>
                                    </div>

                                    <!-- BEGIN :: include alert error section -->
                                    @include('dashboard.components.alert.failed-alert')
                                    <!-- END :: include alert error section -->

                                    <!-- BEGIN :: include alert error section -->
                                    @include('dashboard.components.alert.warning-alert')
                                    <!-- END :: include alert error section -->

                                    <!-- BEGIN :: include alert success section -->
                                    @include('dashboard.components.alert.success-alert')
                                    <!-- END :: include alert success section -->

                                    <div class="leave-form">
                                        <form action="{{ route('front.comments.store', $article->id) }}" method="post" class="leave-comment">
                                            @csrf

                                            <div class="row">
                                                <div class="text col-md-12">
                                                    <textarea name="comment" placeholder="Comment"></textarea>

                                                    @error('comment')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="send">
                                                <button type="submit">Send</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-md-3 col-md-offset-1">
                                    <div class="side-bar">
                                        <div class="news-letters">
                                            <h4>Archives</h4>
                                            <div class="archives-list">
                                                <ul>
                                                    @foreach($categories as $category)
                                                        <li>
                                                            <a href="{{ url("?category=$category->id") }}">
                                                                <i class="fa fa-angle-right"></i>
                                                                {{ $category->name . ' ( ' . $category->articles_count . ' ) ' }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
