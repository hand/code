<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Articles</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link
        href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
        rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/templatemo_style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/templatemo_misc.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/flexslider.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/testimonails-slider.css') }}">
</head>
<body>
<header>
    <div id="top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="home-account">
                        @auth('visitor')
                            <span style="color: #f78e21;">
                                <i class="fa fa-user"></i>
                                {{ auth('visitor')->user()->name }}
                            </span>
                            <span> | </span>
                            <a href="{{ route('front.logout') }}">
                                Logout
                                <i class="fa fa-sign-out"></i>
                            </a>
                        @else
                            <a href="{{ route('front.login') }}">Login</a>
                            <a href="{{ route('front.register') }}">Register</a>
                        @endauth
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="cart-info">
                        <a href="{{ url('/') }}"> Home </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

@yield('content')

<footer>
    <div class="container">
        <div class="bottom-footer"></div>
    </div>
</footer>

<script src="{{ asset('assets/frontend/js/vendor/jquery-1.11.0.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/vendor/jquery.gmap3.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/plugins.js') }}"></script>
<script src="{{ asset('assets/frontend/js/main.js') }}"></script>

</body>
</html>
