@extends('frontend.layouts.master')

@section('content')
    <div id="latest-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-section">
                        <h2>Login page</h2>
                        <img src="{{ asset('assets/frontend/images/under-heading.png') }}" alt="" >
                    </div>
                </div>

                <div class="col-lg-12">
                    <form method="post" action="{{ url('login') }}" class="form-login">
                        @csrf
                        <div class="form-group mb-3">
                            <input type="text"
                                   name="identifier"
                                   class="form-control"
                                   value="{{ old('identifier') }}"
                                   placeholder=" Username Or Email " />

                            @error('identifier')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <input type="password"
                                   name="password"
                                   class="form-control"
                                   placeholder="Password" />

                            @error('password')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <button class="btn btn-submit mt-3" style="background-color: #f78e21;" type="submit"> Login </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
