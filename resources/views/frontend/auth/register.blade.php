@extends('frontend.layouts.master')

@section('content')
    <div id="latest-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-section">
                    <h2>Register page</h2>
                    <img src="{{ asset('assets/frontend/images/under-heading.png') }}" alt="" >
                </div>
            </div>

            <div class="col-lg-12">
                <form action="{{ route('front.register') }}" method="post" class="form-register" enctype="multipart/form-data">
                    @csrf

                    <div class="row">
                        <div class="form-row">
                            <div class="form-group col-md-6 mb-4">
                                <input type="text"
                                       name="name"
                                       value="{{ old('name') }}"
                                       class="form-control"
                                       placeholder="name" />

                                @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group col-md-6 mb-4">
                                <input type="text"
                                       name="username"
                                       value="{{ old('username') }}"
                                       class="form-control"
                                       placeholder=" Username " />

                                @error('username')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6 mb-4">
                                <input type="text"
                                       name="email"
                                       value="{{ old('email') }}"
                                       class="form-control"
                                       placeholder="Email" />

                                @error('email')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group col-md-6 mb-4">
                                <input type="text"
                                       name="phone"
                                       value="{{ old('phone') }}"
                                       class="form-control"
                                       placeholder="Phone" />

                                @error('phone')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                        <div class="form-group col-md-6 mb-4">
                            <input type="password"
                                   name="password"
                                   class="form-control"
                                   placeholder="Password" />

                            @error('password')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group col-md-6 mb-4">
                            <input type="password"
                                   name="password_confirmation"
                                   class="form-control"
                                   placeholder="password confirmation" />
                        </div>
                    </div>
                    </div>

                    <button class="btn btn-submit mt-3" style="background-color: #f78e21;" type="submit"> Register </button>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
