@extends('frontend.layouts.master')

@section('content')
    <div id="latest-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-section">
                        <h2>Latest blog posts</h2>
                        <img src="{{ asset('assets/frontend/images/under-heading.png') }}" alt="">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <form action="{{ route('front.home') }}" class="form-login">
                        <div class="row">
                            <div class="form-group mb-3 col-md-12">
                                <select name="category" class="form-control">
                                    <option value="">-- choose --</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ request()->category ? 'selected' : '' }}>
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-8">
                                <button class="btn btn-submit mt-3" style="background-color: #f78e21;" type="submit"> Search </button>
                                <a href="{{ route('front.home') }}" class="btn btn-light mt-3"> Clear Filter </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                @foreach($articles as $article)
                    <div class="col-md-4 col-sm-6">
                        <div class="blog-post">
                            <div class="blog-thumb">
                                <img src="{{ $article->articleImage }}" alt="{{ $article->title }}"/>
                            </div>

                            <div class="blog-content">
                                <div class="content-show">
                                    <h4><a href={{ route('front.article', [$article->id, $article->slugName]) }}>{{ $article->title }}</a></h4>
                                    <span>{{ $article->created_at->diffForHumans() }}</span>
                                </div>

                                <div class="content-hide">
                                    <p>{{ $article->description }}</p>

                                    <a href="{{ route('front.article', [$article->id, $article->slugName]) }}" class="btn btn-warning">More Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        {{ $articles->links() }}
    </div>
@stop
