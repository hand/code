@extends('dashboard.layouts.master')

@section('title', 'Edit Article')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Edit Category</h1>

    <!-- BEGIN :: include alert error section -->
    @include('dashboard.components.alert.failed-alert')
    <!-- END :: include alert error section -->

    <!-- BEGIN :: include alert error section -->
    @include('dashboard.components.alert.warning-alert')
    <!-- END :: include alert error section -->

    <!-- BEGIN :: include alert success section -->
    @include('dashboard.components.alert.success-alert')
    <!-- END :: include alert success section -->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Create Category</h6>
        </div>
        <div class="card-body">
            <form class="form" method="post" action="{{ route('categories.update', $category->id) }}">
                @csrf
                @method('PUT')

            <!-- BEGIN :: include admin details section -->
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Name</label>

                                <input type="text"
                                       name="name"
                                       placeholder="Name"
                                       id="name"
                                       value="{{ $category->name }}"
                                       class="form-control @error('name') is-invalid @enderror" />

                                @error('name')
                                    <div class="help-block text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <!-- BEGIN :: include create btn group section -->
                @include('dashboard.components.btn.edit-btn-group',['url' => route("categories.index")])
                <!-- END :: include create btn group section -->
            </form>
        </div>
    </div>
    <!-- END : End Main Content-->
@stop
