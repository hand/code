@extends('dashboard.layouts.master')

@section('title', 'Categories')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Categories</h1>

    <!-- BEGIN :: include alert error section -->
    @include('dashboard.components.alert.failed-alert')
    <!-- END :: include alert error section -->

    <!-- BEGIN :: include alert error section -->
    @include('dashboard.components.alert.warning-alert')
    <!-- END :: include alert error section -->

    <!-- BEGIN :: include alert success section -->
    @include('dashboard.components.alert.success-alert')
    <!-- END :: include alert success section -->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Categories</h6>

            <a class="btn btn-raised btn-info btn-min-width mr-1 mb-1 float-right" href="{{ route('categories.create')  }}">
                <i class="fa fa-plus"></i>
                @lang('system.create-new')
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th class="text-center">#ID</th>
                        <th class="text-center">name</th>
                        <th class="text-center">Tools</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($categories as $category)
                            <tr>
                                <td class="text-center">{{ $category->id }}</td>
                                <td class="text-center">{{ $category->name }}</td>
                                <td class="text-center">
                                    <a href="{{ route('categories.edit', $category->id) }}"
                                       class="btn btn-raised btn-info btn-min-width mr-1">
                                        <i class="far fa-edit"></i>
                                    </a>

                                    <!-- BEGIN :: include delete btn -->
                                    @include('dashboard.components.btn.delete-btn', [
                                        'modal_target' => '#delete-category-modal',
                                        'name' => $category->name,
                                        'url' => route('categories.destroy', $category->id),
                                        'btn_id' => 'delete-category'
                                    ])
                                    <!-- END :: include delete btn -->
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td class="text-center"> @lang('messages.there_are_no_data') </td>
                                <td></td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END : End Main Content-->

    <!-- Begin : include delete modal -->
    @include('dashboard.components.modals.delete-modal', [
        'modal_id' => 'delete-category-modal',
        'form_id' => 'delete-category-form',
        'delete_title' => 'Category',
        'btn_delete_id' => '#delete-category'
    ])
    <!-- End : include delete modal -->
@stop
