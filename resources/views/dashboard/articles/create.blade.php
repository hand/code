@extends('dashboard.layouts.master')

@section('title', __('system.create-article'))

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/dashboard/custom/dropify/dist/css/dropify.min.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endpush

@section('content')

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Create Article</h1>

    <!-- BEGIN :: include alert error section -->
    @include('dashboard.components.alert.failed-alert')
    <!-- END :: include alert error section -->

    <!-- BEGIN :: include alert error section -->
    @include('dashboard.components.alert.warning-alert')
    <!-- END :: include alert error section -->

    <!-- BEGIN :: include alert success section -->
    @include('dashboard.components.alert.success-alert')
    <!-- END :: include alert success section -->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Create Article</h6>
        </div>

        <div class="card-body">
            <form class="form" method="post" action="{{ route('articles.store') }}" enctype="multipart/form-data">
                @csrf

                <!-- BEGIN :: include admin details section -->
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="phone">@lang('system.image')</label>

                                <input type="file"
                                       name="article_image"
                                       id="input-file-now-custom-1"
                                       class="dropify @error('article_image') is-invalid @enderror" />

                                @error('article_image')
                                    <small class="help-block text-danger"> {{ $message }} </small>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title">Category</label>
                                <select class="form-select form-control" aria-label="Default select example" name="category_id">
                                    <option value="">-- Choose --</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : ''}}>
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>

                                @error('category_id')
                                    <div class="help-block text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title">@lang('system.title')</label>

                                <input type="text"
                                       name="title"
                                       placeholder="@lang('system.title')"
                                       id="title"
                                       value="{{ old('title') }}"
                                       class="form-control @error('title') is-invalid @enderror" />

                                @error('title')
                                    <div class="help-block text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title">@lang('system.description')</label>

                                <input type="text"
                                       name="description"
                                       placeholder="@lang('system.description')"
                                       id="title"
                                       value="{{ old('description') }}"
                                       class="form-control @error('title') is-invalid @enderror" />

                                @error('description')
                                    <div class="help-block text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title">@lang('system.body')</label>

                                <textarea name="body"
                                          id="summernote"
                                          class="form-control @error('body') is-invalid @enderror">{{ old('body') }}</textarea>

                                @error('body')
                                    <div class="help-block text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <label for="title">Status</label>

                            <div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input"
                                           type="radio"
                                           name="status"
                                           id="inlineRadio1"
                                           value="1" checked />
                                    <label class="form-check-label" for="inlineRadio1">Disabled</label>
                                </div>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input"
                                           type="radio"
                                           name="status"
                                           id="inlineRadio2"
                                           value="2" {{ old('status') == 2 ? 'checked' : '' }} />

                                    <label class="form-check-label" for="inlineRadio2">Active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- BEGIN :: include create btn group section -->
                @include('dashboard.components.btn.create-btn-group',['url' => route("articles.index")])
                <!-- END :: include create btn group section -->
            </form>
        </div>
    </div>
    <!-- END : End Main Content-->
@stop

@push('js')
    <script src="{{ asset('assets/dashboard/custom/dropify/dist/js/dropify.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

    <script>
        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();

            $('#summernote').summernote({
                placeholder: '{{ __('system.body') }}',
                tabsize: 2,
                height: 250,
            });
        });
    </script>
@endpush

