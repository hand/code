@extends('dashboard.layouts.master')

@section('title', __('system.articles'))

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Articles</h1>

    <!-- BEGIN :: include alert error section -->
    @include('dashboard.components.alert.failed-alert')
    <!-- END :: include alert error section -->

    <!-- BEGIN :: include alert error section -->
    @include('dashboard.components.alert.warning-alert')
    <!-- END :: include alert error section -->

    <!-- BEGIN :: include alert success section -->
    @include('dashboard.components.alert.success-alert')
    <!-- END :: include alert success section -->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Articles</h6>

            <a class="btn btn-raised btn-info btn-min-width mr-1 mb-1 float-right" href="{{ route('articles.create')  }}">
                <i class="fa fa-plus"></i>
                @lang('system.create-new')
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th class="text-center">#ID</th>
                        <th class="text-center">Image</th>
                        <th class="text-center">Category</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Tools</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($articles as $article)
                            <tr>
                                <td class="text-center">{{ $article->id }}</td>
                                <td class="text-center">
                                    <img class="media-object round-media" src="{{ $article->articleImage }}" alt="{{ $article->title }}" style="height: 50px;width: 75px">
                                </td>
                                <td class="text-center">{{ $article->category->name }}</td>
                                <td class="text-center">{{ $article->title }}</td>
                                <td class="text-center">
                                    <button class="btn btn-{{  $article->statusName['class'] }}">
                                        {{  $article->statusName['name'] }}
                                    </button>
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('articles.edit', $article->id) }}"
                                       class="btn btn-raised btn-info btn-min-width mr-1">
                                        <i class="far fa-edit"></i>
                                    </a>

                                    <!-- BEGIN :: include delete btn -->
                                    @include('dashboard.components.btn.delete-btn', [
                                        'modal_target' => '#delete-article-modal',
                                        'name' => $article->name,
                                        'url' => route('articles.destroy',$article->id),
                                        'btn_id' => 'delete-article'
                                    ])
                                    <!-- END :: include delete btn -->
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td class="text-center" colspan="2"> @lang('messages.there_are_no_data') </td>
                                <td></td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END : End Main Content-->

    <!-- Begin : include delete modal -->
    @include('dashboard.components.modals.delete-modal', [
        'modal_id' => 'delete-article-modal',
        'form_id' => 'delete-article-form',
        'delete_title' => __('system.article'),
        'btn_delete_id' => '#delete-article'
    ])
    <!-- End : include delete modal -->
@stop
