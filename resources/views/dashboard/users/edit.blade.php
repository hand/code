@extends('dashboard.layouts.master')

@section('title', __('system.edit-user'))

@section('content')
    <div class="main-content">
        <div class="content-wrapper">
            <section id="basic-form-layouts">

                <!-- BEGIN :: include page content header -->
                @include('dashboard.components.pages.content-header', ['title' => __('system.edit-user')])
                <!-- END :: include page content header -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <!-- BEGIN :: include alert error section -->
                                @include('dashboard.components.alert.failed-alert')
                                <!-- END :: include alert error section -->

                                <!-- BEGIN :: include alert error section -->
                                @include('dashboard.components.alert.warning-alert')
                                <!-- END :: include alert error section -->

                                <!-- BEGIN :: include alert success section -->
                                @include('dashboard.components.alert.success-alert')
                                <!-- END :: include alert success section -->
                            </div>

                            <div class="card-content">
                                <div class="px-3">
                                    <form class="form" method="post" action="{{ route('users.update', $user->id) }}">
                                        @csrf
                                        {{ method_field('PUT') }}

                                        <div class="form-body">
                                            <h4 class="form-section">
                                                <i class="fa fa-user"></i>
                                                @lang('system.user-information')
                                            </h4>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">@lang('system.name')</label>

                                                        <input type="text"
                                                               name="name"
                                                               placeholder="@lang('system.name')"
                                                               id="name"
                                                               value="{{ $user->name }}"
                                                               class="form-control @error('name') is-invalid @enderror" />

                                                        @error('name')
                                                            <div class="help-block text-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="phone">@lang('system.phone')</label>

                                                        <input type="text"
                                                               name="phone"
                                                               placeholder="@lang('system.phone')"
                                                               id="phone"
                                                               value="{{ $user->phone }}"
                                                               class="form-control @error('phone') is-invalid @enderror" />

                                                        @error('phone')
                                                            <div class="help-block text-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="email">@lang('system.email')</label>

                                                        <input type="email"
                                                               name="email"
                                                               placeholder="@lang('system.email')"
                                                               id="email"
                                                               value="{{ $user->email }}"
                                                               class="form-control @error('email') is-invalid @enderror" />

                                                        @error('email')
                                                            <div class="help-block text-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="username">@lang('system.username')</label>

                                                        <input type="text"
                                                               name="username"
                                                               placeholder="@lang('system.username')"
                                                               id="username"
                                                               value="{{ $user->username }}"
                                                               class="form-control @error('username') is-invalid @enderror" />

                                                        @error('username')
                                                            <div class="help-block text-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- BEGIN :: include edit btn group section -->
                                        @include('dashboard.components.btn.edit-btn-group',['url' => route("users.index")])
                                        <!-- END :: include edit btn group section -->
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <div class="card-content">
                                    <form class="form" method="post" action="{{ route('user.update.password', $user->id) }}">
                                        @csrf
                                        {{ method_field('PUT') }}

                                        <h4 class="form-section">
                                            <i class="fa fa-info"></i>
                                            @lang('system.password')
                                        </h4>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="password">@lang('system.password')</label>

                                                    <input type="password"
                                                           name="password"
                                                           placeholder="@lang('system.password')"
                                                           id="password"
                                                           class="form-control @error('password') is-invalid @enderror" />

                                                    @error('password')
                                                        <div class="help-block text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="password_confirmation">@lang('system.password-confirmation')</label>

                                                    <input type="password"
                                                           name="password_confirmation"
                                                           placeholder="@lang('system.password-confirmation')"
                                                           id="password_confirmation"
                                                           class="form-control @error('password_confirmation') is-invalid @enderror" />

                                                    @error('password_confirmation')
                                                    <div class="help-block text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <!-- BEGIN :: include edit btn group section -->
                                        @include('dashboard.components.btn.edit-btn-group',['url' => route('users.index')])
                                        <!-- END :: include edit btn group section -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
