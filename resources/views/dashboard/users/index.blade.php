@extends('dashboard.layouts.master')

@section('title', __('system.all-users'))

@section('content')

    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-wrapper">

            <!-- BEGIN :: include content header -->
            @include('dashboard.components.pages.content-header',['title' => __('system.all-users')])
            <!-- END :: include content header -->

            <!--Shopping cart starts-->
            <section id="shopping-cart">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <!-- BEGIN :: include alert error section -->
                                @include('dashboard.components.alert.failed-alert')
                                <!-- END :: include alert error section -->

                                <!-- BEGIN :: include alert error section -->
                                @include('dashboard.components.alert.warning-alert')
                                <!-- END :: include alert error section -->

                                <!-- BEGIN :: include alert success section -->
                                @include('dashboard.components.alert.success-alert')
                                <!-- END :: include alert success section -->

                                <h4 class="card-title" style="display: inline">@lang('system.all-users')</h4>

                                <a class="btn btn-raised btn-info btn-min-width mr-1 mb-1 float-right" href="{{ route('users.create')  }}">
                                    <i class="fa fa-plus"></i>
                                    @lang('system.create-new')
                                </a>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <table class="table table-responsive-md text-center">
                                        <thead>
                                        <tr>
                                            <th>#ID</th>
                                            <th>@lang('system.name')</th>
                                            <th>@lang('system.phone')</th>
                                            <th>@lang('system.email')</th>
                                            <th>@lang('system.tools')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($users as $user)
                                                <tr>
                                                    <td>{{ $user->id }}</td>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->phone }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>
                                                        <a href="{{ route('users.edit',$user->id) }}"
                                                                class="btn btn-raised btn-info btn-min-width mr-1">
                                                            <i class="far fa-edit"></i>
                                                        </a>

                                                        @if(auth()->user()->id != $user->id)
                                                            <!-- BEGIN :: include delete btn -->
                                                            @include('dashboard.components.btn.delete-btn', [
                                                                'modal_target' => '#delete-admin-modal',
                                                                'name' => $user->name,
                                                                'url' => route('users.destroy',$user->id),
                                                                'btn_id' => 'delete-admin'
                                                            ])
                                                            <!-- END :: include delete btn -->
                                                        @else
                                                            <button type="button"
                                                                    data-placement="top"
                                                                    data-toggle="tooltip"
                                                                    title="@lang('system.cannot_deleted_your_account')"
                                                                    class="btn btn-raised btn-icon btn-danger mr-1">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td> @lang('messages.there_are_no_data') </td>
                                                    <td colspan="2"></td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>

                                    {{ $users->appends(Request::except('page'))->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- END : End Main Content-->

    <!-- Begin : include delete modal -->
    @include('dashboard.components.modals.delete-modal', [
        'modal_id' => 'delete-admin-modal',
        'form_id' => 'delete-admin-form',
        'delete_title' => __('system.users'),
        'btn_delete_id' => '#delete-admin'
    ])
    <!-- End : include delete modal -->
@endsection
