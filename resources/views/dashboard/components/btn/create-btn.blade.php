<button type="button"
        onclick="location.href='{{ $url ?? '#!' }}'"
        class="btn btn-raised btn-info btn-min-width mr-1 mb-1 float-right">
    <i class="fa fa-plus"></i>
    {{ $title ?? '' }}
</button>
