<div class="form-actions">
    <button type="submit"
            name="after"
            value="close"
            class="btn btn-raised btn-raised btn-info">
        <i class="fa fa-check-square-o"></i> @lang('system.add-then-close')
    </button>

    <button type="submit"
            name="after"
            value="new"
            class="btn btn-raised btn-raised btn-primary">
        <i class="fa fa-repeat"></i> @lang('system.add-then-add-new')
    </button>

    <button type="button"
            onclick="location.href='{{ $url }}'"
            class="btn btn-raised btn-raised btn-light mr-1">
        <i class="ft-x"></i>  @lang('system.cancel')
    </button>
</div>
