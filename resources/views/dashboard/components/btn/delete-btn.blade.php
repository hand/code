<span data-toggle="modal" data-target="{{ $modal_target }}">
    <button type="button"
            data-placement="top"
            data-toggle="tooltip"
            title="{{ __('system.delete') . ' ' . $name }} "
            data-url="{{ $url }}"
            id="{{ $btn_id }}"
            class="btn btn-raised btn-icon btn-danger mr-1">
        <i class="fa fa-trash"></i>
    </button>
</span>
