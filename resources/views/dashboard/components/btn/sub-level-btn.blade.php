<button type="button"
        onclick="location.href='{{ $url }}'"
        class="btn btn-raised btn-icon btn-primary mr-1">
    {{ $title }}
    <i class="ft-corner-down-left"></i>
</button>
