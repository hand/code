<div class="form-actions">
    <button type="submit"
            class="btn btn-raised btn-raised btn-info">
        <i class="fa fa-pencil"></i>
        @lang('system.edit')
    </button>

    <a href="{{ $url }}"
       class="btn btn-raised btn-raised btn-warning mr-1">
        <i class="ft-x"></i>
        @lang('system.cancel')
    </a>
</div>
