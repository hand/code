<div class="modal fade" id="{{ $modal_id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form method="post" action="#" id="{{ $form_id }}">
            @csrf
            {{ method_field('DELETE') }}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> {{ __('system.delete') . ' ' . $delete_title }}</h5>
                </div>
                <div class="modal-body">
                    <p>@lang('system.msgForDelete')</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('system.close')</button>
                    <button type="submit" class="btn btn-danger">@lang('system.delete')</button>
                </div>
            </div>
        </form>
    </div>
</div>

@push('js')
    <script src="{{ asset('assets/admin/js/components-modal.min.js') }}" type="text/javascript"></script>

    <!-- Begin :: Script To Change Modal Delete -->
    <script type="text/javascript">
        $(document).on('click', "{{ $btn_delete_id }}", function () {
            $("#{{ $form_id }}").attr('action', $(this).data('url'));
        })
    </script>
    <!-- End :: Script To Change Modal Delete -->
@endpush
