@if(\Session::has('warning'))
    <div class="alert alert-danger alert-dismissible p-t-20" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="right:unset !important;left: 0 !important;">
            <span aria-hidden="true">×</span>
        </button>
        {{ \Session::get('warning')}}
    </div>
@endif
