<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ url('admin') }}">الصفحة الرئيسية </a>
            </li>

            @if(isset($extra_breadcrumb_name) && isset($extra_breadcrumb_url))
                <li class="breadcrumb-item">
                    <a href="{{ $extra_breadcrumb_url }}">
                        {{ $extra_breadcrumb_name }}
                    </a>
                </li>
            @endif

            <li class="breadcrumb-item active">{{ $page_title ?? 'صفحة جانبية' }}</li>
        </ol>
    </div>

    @if(isset($url))
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <button type="button"
                        onclick="location.href='{{ $url }}'"
                        class="btn btn-info d-none d-lg-block m-l-15">

                    <i class="{{ $icon }}"></i>
                    {{ $btn_create_title }}
                </button>
            </div>
        </div>
    @endif
</div>
