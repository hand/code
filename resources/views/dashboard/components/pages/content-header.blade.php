<div class="row">
    <div class="col-sm-12">
        <div class="content-header">{{ $title ?? 'page' }}</div>
        <p class="content-sub-header">{{ $sub_title ?? '' }}</p>
    </div>
</div>
