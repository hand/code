@extends('dashboard.layouts.master')

@section('title', 'All Comments')

@section('content')

    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-wrapper">

            <!-- BEGIN :: include content header -->
            @include('dashboard.components.pages.content-header', ['title' => 'All Comments'])
            <!-- END :: include content header -->

            <!--Shopping cart starts-->
            <section id="shopping-cart">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <!-- BEGIN :: include alert error section -->
                                @include('dashboard.components.alert.failed-alert')
                                <!-- END :: include alert error section -->

                                <!-- BEGIN :: include alert error section -->
                                @include('dashboard.components.alert.warning-alert')
                                <!-- END :: include alert error section -->

                                <!-- BEGIN :: include alert success section -->
                                @include('dashboard.components.alert.success-alert')
                                <!-- END :: include alert success section -->

                                <a class="btn btn-raised btn-info btn-min-width mr-1 mb-1 float-right" href="{{ route('comments.create')  }}">
                                    <i class="fa fa-plus"></i>
                                    @lang('system.create-new')
                                </a>
                            </div>

                            <div class="card-content">
                                <div class="card-body">
                                    <table class="table table-responsive-md text-center">
                                        <thead>
                                        <tr>
                                            <th>#ID</th>
                                            <th>@lang('system.name')</th>
                                            <th>created at</th>
                                            <th>@lang('system.tools')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($comments as $comment)
                                                <tr>
                                                    <td>{{ $comment->id }}</td>

                                                    <td>{{ $comment->name }}</td>
                                                    <td>{{ $comment->created_at->diffForHumans() }}</td>

                                                    <td>
                                                        <a href="{{ route('comments.edit', $comment->id) }}"
                                                           class="btn btn-raised btn-info btn-min-width mr-1">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>

                                                        <!-- BEGIN :: include delete btn -->
                                                        @include('dashboard.components.btn.delete-btn', [
                                                            'modal_target' => '#delete-comment-modal',
                                                            'name' => $comment->name,
                                                            'url' => route('comments.destroy', $comment->id),
                                                            'btn_id' => 'delete-comment'
                                                        ])
                                                        <!-- END :: include delete btn -->
                                                    </td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td></td>
                                                    <td colspan="2"> @lang('messages.there_are_no_data') </td>
                                                    <td></td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>

                                    {{ $comments->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- END : End Main Content-->

    <!-- Begin : include delete modal -->
    @include('dashboard.components.modals.delete-modal', [
        'modal_id' => 'delete-comment-modal',
        'form_id' => 'delete-comment-form',
        'delete_title' => 'comment',
        'btn_delete_id' => '#delete-comment'
    ])
    <!-- End : include delete modal -->
@stop
