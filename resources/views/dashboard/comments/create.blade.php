@extends('dashboard.layouts.master')

@section('title', 'Create Comment')

@section('content')
    <div class="main-content">
        <div class="content-wrapper">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">

                <!-- BEGIN :: include page content header -->
                @include('dashboard.components.pages.content-header', ['title' => 'Create Comment'])
                <!-- END :: include page content header -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <!-- BEGIN :: include alert error section -->
                                @include('dashboard.components.alert.failed-alert')
                                <!-- END :: include alert error section -->

                                <!-- BEGIN :: include alert error section -->
                                @include('dashboard.components.alert.warning-alert')
                                <!-- END :: include alert error section -->

                                <!-- BEGIN :: include alert success section -->
                                @include('dashboard.components.alert.success-alert')
                                <!-- END :: include alert success section -->
                            </div>

                            <div class="card-content">
                                <div class="px-3">
                                    <form class="form" method="post" action="{{ route('comments.store') }}">
                                        @csrf

                                        <!-- BEGIN :: include admin details section -->
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="name">@lang('system.name')</label>

                                                        <input type="text"
                                                               name="name"
                                                               placeholder="@lang('system.name')"
                                                               id="name"
                                                               value="{{ old('name') }}"
                                                               class="form-control @error('name') is-invalid @enderror" />

                                                        @error('name')
                                                            <div class="help-block text-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="job_title">content</label>

                                                        <textarea name="content"
                                                                  class="form-control @error('content') is-invalid @enderror"
                                                                  cols="30"
                                                                  rows="3">{{ old('content') }}</textarea>

                                                        @error('content')
                                                            <div class="help-block text-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END :: include admin details section -->

                                        <!-- BEGIN :: include create btn group section -->
                                        @include('dashboard.components.btn.create-btn-group',['url' => route("comments.index")])
                                        <!-- END :: include create btn group section -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@stop
