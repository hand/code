@extends('dashboard.layouts.master')

@section('title', 'Create Visitor')

@section('content')
    <div class="main-content">
        <div class="content-wrapper">
            <!-- Basic form layout section start -->
            <section id="basic-form-layouts">

                <!-- BEGIN :: include page content header -->
                @include('dashboard.components.pages.content-header', ['title' => 'Create Visitor'])
                <!-- END :: include page content header -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <!-- BEGIN :: include alert error section -->
                                @include('dashboard.components.alert.failed-alert')
                                <!-- END :: include alert error section -->

                                <!-- BEGIN :: include alert error section -->
                                @include('dashboard.components.alert.warning-alert')
                                <!-- END :: include alert error section -->

                                <!-- BEGIN :: include alert success section -->
                                @include('dashboard.components.alert.success-alert')
                                <!-- END :: include alert success section -->
                            </div>

                            <div class="card-content">
                                <div class="px-3">
                                    <form class="form" method="post" action="{{ route('visitors.store') }}">
                                        @csrf

                                        <!-- BEGIN :: include admin details section -->
                                        <div class="form-body">
                                                <h4 class="form-section">
                                                    <i class="fa fa-user"></i>
                                                    Visitor information
                                                </h4>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="name">@lang('system.name')</label>

                                                            <input type="text"
                                                                   name="name"
                                                                   placeholder="@lang('system.name')"
                                                                   id="name"
                                                                   value="{{ old('name') }}"
                                                                   class="form-control @error('name') is-invalid @enderror" />

                                                            @error('name')
                                                                <div class="help-block text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="phone">@lang('system.phone')</label>

                                                            <input type="text"
                                                                   name="phone"
                                                                   placeholder="@lang('system.phone')"
                                                                   id="phone"
                                                                   value="{{ old('phone') }}"
                                                                   class="form-control @error('phone') is-invalid @enderror" />

                                                            @error('phone')
                                                                <div class="help-block text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email">@lang('system.email')</label>

                                                            <input type="email"
                                                                   name="email"
                                                                   placeholder="@lang('system.email')"
                                                                   id="email"
                                                                   value="{{ old('email') }}"
                                                                   class="form-control @error('email') is-invalid @enderror" />

                                                            @error('email')
                                                                <div class="help-block text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="username">@lang('system.username')</label>

                                                            <input type="text"
                                                                   name="username"
                                                                   placeholder="@lang('system.username')"
                                                                   id="username"
                                                                   value="{{ old('username') }}"
                                                                   class="form-control @error('username') is-invalid @enderror" />

                                                            @error('username')
                                                                <div class="help-block text-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        <!-- END :: include admin details section -->

                                        <h4 class="form-section"><i class="fa fa-user-secret"></i> @lang('system.password-data') </h4>

                                        <br/>

                                        <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="password">@lang('system.password')</label>

                                                        <input type="password"
                                                               name="password"
                                                               placeholder="@lang('system.password')"
                                                               id="password"
                                                               class="form-control @error('password') is-invalid @enderror" />

                                                        @error('password')
                                                            <div class="help-block text-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="password_confirmation">@lang('system.password-confirmation')</label>

                                                        <input type="password"
                                                               name="password_confirmation"
                                                               placeholder="@lang('system.password-confirmation')"
                                                               id="password_confirmation"
                                                               class="form-control @error('password_confirmation') is-invalid @enderror" />

                                                        @error('password_confirmation')
                                                            <div class="help-block text-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                        <!-- END :: include admin password section -->

                                        <!-- BEGIN :: include create btn group section -->
                                        @include('dashboard.components.btn.create-btn-group',['url' => route("visitors.index")])
                                        <!-- END :: include create btn group section -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- // Basic form layout section end -->
        </div>
    </div>
@endsection
