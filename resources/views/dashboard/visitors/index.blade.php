@extends('dashboard.layouts.master')

@section('title', 'All Visitors')

@section('content')

    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-wrapper">

            <!-- BEGIN :: include content header -->
            @include('dashboard.components.pages.content-header',['title' => 'All Visitors'])
            <!-- END :: include content header -->

            <!--Shopping cart starts-->
            <section id="shopping-cart">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <!-- BEGIN :: include alert error section -->
                                @include('dashboard.components.alert.failed-alert')
                                <!-- END :: include alert error section -->

                                <!-- BEGIN :: include alert error section -->
                                @include('dashboard.components.alert.warning-alert')
                                <!-- END :: include alert error section -->

                                <!-- BEGIN :: include alert success section -->
                                @include('dashboard.components.alert.success-alert')
                                <!-- END :: include alert success section -->

                                <h4 class="card-title" style="display: inline">All Visitors</h4>

                                <a class="btn btn-raised btn-info btn-min-width mr-1 mb-1 float-right" href="{{ route('visitors.create')  }}">
                                    <i class="fa fa-plus"></i>
                                    @lang('system.create-new')
                                </a>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <table class="table table-responsive-md text-center">
                                        <thead>
                                        <tr>
                                            <th>#ID</th>
                                            <th>@lang('system.name')</th>
                                            <th>@lang('system.phone')</th>
                                            <th>@lang('system.email')</th>
                                            <th>@lang('system.tools')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($visitors as $visitor)
                                                <tr>
                                                    <td>{{ $visitor->id }}</td>
                                                    <td>{{ $visitor->name }}</td>
                                                    <td>{{ $visitor->phone }}</td>
                                                    <td>{{ $visitor->email }}</td>
                                                    <td>
                                                        <a href="{{ route('visitors.edit', $visitor->id) }}"
                                                                class="btn btn-raised btn-info btn-min-width mr-1">
                                                            <i class="far fa-edit"></i>
                                                        </a>

                                                        <!-- BEGIN :: include delete btn -->
                                                        @include('dashboard.components.btn.delete-btn', [
                                                            'modal_target' => '#delete-visitor-modal',
                                                            'name' => $visitor->name,
                                                            'url' => route('visitors.destroy', $visitor->id),
                                                            'btn_id' => 'delete-visitor'
                                                        ])
                                                        <!-- END :: include delete btn -->
                                                    </td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td> @lang('messages.there_are_no_data') </td>
                                                    <td colspan="2"></td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>

                                    {{ $visitors->appends(Request::except('page'))->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- END : End Main Content-->

    <!-- Begin : include delete modal -->
    @include('dashboard.components.modals.delete-modal', [
        'modal_id' => 'delete-visitor-modal',
        'form_id' => 'delete-visitor-form',
        'delete_title' => 'Visitor',
        'btn_delete_id' => '#delete-visitor'
    ])
    <!-- End : include delete modal -->
@endsection
