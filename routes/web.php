<?php

use App\Http\Controllers\Frontend\Articles\ArticlesController;
use App\Http\Controllers\Frontend\Auth\LoginController;
use App\Http\Controllers\Frontend\Auth\RegisterController;
use App\Http\Controllers\Frontend\Comments\CommentsController;
use App\Http\Controllers\Frontend\Home\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

# BEGIN :: login routes.
Route::get('login', [LoginController::class, 'showLoginForm'])->name('front.login');
Route::post('login', [LoginController::class, 'login']);

Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('front.register');
Route::post('register', [RegisterController::class, 'register']);
Route::any('logout', [LoginController::class, 'logout'])->name('front.logout');

Route::get('/', [HomeController::class, 'index'])->name('front.home');

# Article routes.
Route::get('article/{article}/{slug}', [ArticlesController::class, 'show'])->name('front.article');

Route::group(['middleware' => 'auth:visitor'], function (){
    Route::post('comment/{article}', [CommentsController::class, 'store'])->name('front.comments.store');
});
