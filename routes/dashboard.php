<?php

use App\Http\Controllers\Dashboard\Articles\ArticlesController;
use App\Http\Controllers\Dashboard\Auth\LoginController;
use App\Http\Controllers\Dashboard\Categories\CategoriesController;
use App\Http\Controllers\Dashboard\Users\UsersController;
use App\Http\Controllers\Dashboard\Visitors\VisitorsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register dashboard routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

# Login routes.
Route::get('login', [LoginController::class, 'showLoginForm'])->name('admin.login');
Route::post('login', [LoginController::class, 'login'])->name('admin.login');
Route::any('logout', [LoginController::class, 'logout'])->name('admin.logout');

# Dashboard group routes.
Route::group(['middleware' => ['auth']], function () {

    # Dashboard route.
    Route::get('/', function () {
        return view('dashboard.home.home');
    })->name('admin.home');

    # Users routes.
    Route::resource('users', UsersController::class)->except('show');
    Route::put('users/update/password/{user}', [UsersController::class, 'updatePassword'])->name('user.update.password');

    # Visitors routes.
    Route::resource('visitors', VisitorsController::class)->except('show');
    Route::put('visitors/update/password/{visitor}', [VisitorsController::class, 'updatePassword'])->name('visitor.update.password');

    # Articles routes.
    Route::resource('articles', ArticlesController::class)->except('show');

    # Categories routes.
    Route::resource('categories', CategoriesController::class)->except('show');
});
