<?php

namespace App\Models;

use App\Enums\GeneralEnums;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Article extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $appends = ['article_image'];

    protected $fillable = [
        'title',
        'description',
        'body',
        'category_id',
        'status' # 1 => DISABLED | 2 => ACTIVE.
    ];

    /**
     * Return article image.
     *
     * @return string $imagePath
     */
    public function getArticleImageAttribute(): string
    {
        if ($image = $this->getFirstMediaUrl('article_image')) {
            return $image;
        }

        return Url(GeneralEnums::DEFAULT_IMAGE_PLACEHOLDER);
    }

    /**
     * Get status details.
     *
     * @return string[]
     */
    public function getStatusNameAttribute(): array
    {
        return $this->status == 1 ? ['name' => 'Disabled', 'class' => 'warning'] : ['name' => 'Active', 'class' => 'success'];
    }

    /**
     * Category relation.
     *
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Comments relation.
     *
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class)->with('visitor');
    }

    /**
     * Get article slug.
     *
     * @return string
     */
    public function getSlugNameAttribute(): string
    {
        return Str::slug($this->title);
    }
}
