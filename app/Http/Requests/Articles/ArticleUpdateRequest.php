<?php

namespace App\Http\Requests\Articles;

use App\Enums\GeneralEnums;
use Illuminate\Foundation\Http\FormRequest;

class ArticleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'article_image' => ['nullable', 'image', 'max:3000', 'mimes:' . GeneralEnums::mimesType],
            'category_id' => ['required', 'numeric', 'exists:categories,id'],
            'title' => ['required', 'max:255'],
            'description' => ['required', 'string', 'max:400'],
            'body' => ['required'],
        ];
    }
}
