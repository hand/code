<?php

namespace App\Http\Requests\Articles;

use App\Enums\GeneralEnums;
use Illuminate\Foundation\Http\FormRequest;

class ArticleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'article_image' => ['required', 'image', 'max:3000', 'mimes:' . GeneralEnums::mimesType],
            'category_id' => ['required', 'numeric', 'exists:categories,id'],
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'body' => ['required', 'string'],
        ];
    }
}
