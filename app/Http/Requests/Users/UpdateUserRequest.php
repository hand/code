<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:60'],
            'username' => ['required', 'string', 'max:60','unique:users,username,' . collect(request()->segments())->last()],
            'phone' => ['required', 'string', 'max:20', 'unique:users,phone,' . collect(request()->segments())->last()],
            'email' => ['required', 'email', 'max:150', 'unique:users,email,' . collect(request()->segments())->last()],
        ];
    }
}
