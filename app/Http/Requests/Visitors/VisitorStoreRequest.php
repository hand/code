<?php

namespace App\Http\Requests\Visitors;

use Illuminate\Foundation\Http\FormRequest;

class VisitorStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:60'],
            'username' => ['required', 'string', 'max:60', 'unique:visitors,username'],
            'phone' => ['required', 'string', 'max:20', 'unique:visitors,phone'],
            'email' => ['required', 'email', 'max:150', 'unique:visitors,email'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }
}
