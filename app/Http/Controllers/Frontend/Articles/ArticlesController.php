<?php

namespace App\Http\Controllers\Frontend\Articles;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Repositories\Articles\ArticlesRepositories;
use App\Repositories\Categories\CategoriesRepository;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    /**
     * Get page to display articles details.
     *
     * @param Article $article
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Article $article, $slug)
    {
        # Repository to list categories.
        $categories = app(CategoriesRepository::class)->list();

        return view('frontend.articles.show', compact('article', 'categories'));
    }
}
