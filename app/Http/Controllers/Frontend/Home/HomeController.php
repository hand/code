<?php

namespace App\Http\Controllers\Frontend\Home;

use App\Http\Controllers\Controller;
use App\Repositories\Articles\ArticlesRepositories;
use App\Repositories\Categories\CategoriesRepository;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(Request $request)
    {
        # Repository to list articles.
        $articles = app(ArticlesRepositories::class)->list($request->all());

        # Repository to list categories.
        $categories = app(CategoriesRepository::class)->list();

        return view('frontend.home.index', compact('articles', 'categories'));
    }
}
