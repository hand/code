<?php

namespace App\Http\Controllers\Frontend\Comments;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comments\CommentStoreRequest;
use App\Models\Article;
use App\Repositories\Comments\CommentRepository;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    protected $commentRepository;

    /**
     * CommentsController constructor.
     *
     * @param CommentRepository $commentRepository
     */
    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * Store a newly created comment in storage.
     *
     * @param CommentStoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CommentStoreRequest $request, Article $article)
    {
        $data = array_merge($request->only('comment'),
            ['article_id' => $article->id, 'visitor_id' => auth('visitor')->user()->id]);

        if( !$this->commentRepository->create($data) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        return redirect()->route('front.article', [$article->id, $article->slugName])->with('success', __('system.created'));
    }
}
