<?php

namespace App\Http\Controllers\Dashboard\Categories;

use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\CategoryStoreRequest;
use App\Http\Requests\Categories\CategoryUpdateRequest;
use App\Models\Category;
use App\Repositories\Categories\CategoriesRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class CategoriesController extends Controller
{
    protected $model;

    /**
     * CategoriesRepository constructor.
     *
     * @param CategoriesRepository $categoriesRepository
     */
    public function __construct(CategoriesRepository $categoriesRepository)
    {
        $this->model = $categoriesRepository;
    }

    /**
     * Display a listing of the categories.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        # List categories.
        $categories = $this->model->get();

        return view('dashboard.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new category.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('dashboard.categories.create');
    }

    /**
     * Store a newly created category in storage.
     *
     * @param CategoryStoreRequest $request
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function store(CategoryStoreRequest $request)
    {
        # check if repository not create category return alert.
        if( !$this->model->create( $request->except('_token', 'after') ) )
            return redirect()->back()->with('failed', __('messages.field_process'));

        # check if submit on btn create & new
        if ($request->get('after') == 'new')
            return redirect()->route('categories.create')->with('success', __('system.created'));

        return redirect()->route('categories.index')->with('success', __('system.created'));
    }

    /**
     * Display the specified specific.
     *
     * @param int $id
     */
    public function show(int $id)
    {
        //
    }

    /**
     * Show the form for editing the specified category.
     *
     * @param Category $category
     * @return Application|Factory|View
     */
    public function edit(Category $category)
    {
        return view('dashboard.categories.edit', compact('category'));
    }

    /**
     * Update the specified category in storage.
     *
     * @param Category $category
     * @param CategoryUpdateRequest $request
     * @return Application|RedirectResponse|Redirector
     */
    public function update(CategoryUpdateRequest $request, Category $category)
    {
        # check if repository not update category return alert.
        if( !$this->model->update($category, $request->except('_token', '_method')) )
            return redirect()->back()->with('failed', __('messages.field_process'));

        return redirect()->back()->with('success', __('system.updated'));
    }

    /**
     * Remove the specified category from storage.
     *
     * @param Category $category
     * @return Application|RedirectResponse|Redirector
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        # check if repository not delete category return alert.
        if( !$this->model->delete($category) )
            return redirect()->back()->with('failed', __('messages.field_process'));

        return redirect()->back()->with('success', __('system.deleted'));
    }
}
