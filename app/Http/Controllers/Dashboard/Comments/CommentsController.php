<?php

namespace App\Http\Controllers\Dashboard\Comments;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comments\CommentStoreRequest;
use App\Http\Requests\Comments\CommentUpdateRequest;
use App\Models\Comment;
use App\Repositories\Comments\CommentRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    protected $commentsRepository;

    /**
     * Comment Repository constructor.
     *
     * @param CommentRepository $commentsRepository
     */
    public function __construct(CommentRepository $commentsRepository)
    {
        $this->commentsRepository = $commentsRepository;
    }

    /**
     * Display a listing of the comments.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        # List comments.
        $comments = $this->commentsRepository->get();

        return view('dashboard.comments.index', compact('comments'));
    }

    /**
     * Show the form for creating a new comment.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('dashboard.comments.create');
    }

    /**
     * Store a newly created comment in storage.
     *
     * @param CommentStoreRequest $request
     * @return RedirectResponse
     */
    public function store(CommentStoreRequest $request)
    {
        # check if repository not create comment return alert.
        if( !$this->commentsRepository->create( $request->except('_token', 'after') ) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        # check if submit on btn create & new
        if ($request->get('after') == 'new'){
            return redirect()->route('comments.create')->with('success', __('system.created'));
        }

        return redirect()->route('comments.index')->with('success', __('system.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified comment.
     *
     * @param Comment $comment
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Comment $comment)
    {
        return view('dashboard.comments.edit', compact('comment'));
    }

    /**
     * Update the specified comment in storage.
     *
     * @param CommentUpdateRequest $request
     *
     * @param Comment $comment
     *
     * @return RedirectResponse
     */
    public function update(CommentUpdateRequest $request, Comment $comment)
    {
        # check if repository not update comment return alert.
        if( !$this->commentsRepository->update($comment, $request->except('_token', '_method')) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        return redirect()->route('comments.index')->with('success', __('system.updated'));
    }

    /**
     * Remove the specified comment from storage.
     *
     * @param Comment $comment
     *
     * @return RedirectResponse
     */
    public function destroy(Comment $comment)
    {
        # check if repository not delete board return alert.
        if( !$this->commentsRepository->delete($comment) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        return redirect()->route('comments.index')->with('success', __('system.deleted'));
    }
}
