<?php

namespace App\Http\Controllers\Dashboard\Visitors;

use App\Http\Controllers\Controller;
use App\Http\Requests\Visitors\UpdateUserPasswordRequest;
use App\Http\Requests\Visitors\VisitorStoreRequest;
use App\Http\Requests\Visitors\VisitorUpdateRequest;
use App\Models\Visitor;
use App\Repositories\Visitors\VisitorsRepository;
use Illuminate\Http\Request;

class VisitorsController extends Controller
{
    private $visitorsRepository;

    /**
     * VisitorsController constructor.
     *
     * @param VisitorsRepository $visitorsRepository
     */
    public function __construct(VisitorsRepository $visitorsRepository)
    {
        $this->visitorsRepository = $visitorsRepository;
    }

    /**
     * Display a listing of the visitors.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        # Repository to get all visitors.
        $visitors = $this->visitorsRepository->get();

        return view('dashboard.visitors.index', compact('visitors'));
    }

    /**
     * Show the form for creating a new visitor.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dashboard.visitors.create');
    }

    /**
     * Store a newly created visitor in storage.
     *
     * @param VisitorStoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(VisitorStoreRequest $request)
    {
        # check if repository not create visitor return alert.
        if( !$this->visitorsRepository->create( $request->except('_token', 'after', 'password_confirmation') ) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        # check if submit on btn create & new
        if ($request->get('after') == 'new')
            return redirect()->route('visitors.create')->with('success', __('system.created'));

        return redirect()->route('visitors.index')->with('success', __('system.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show(int $id)
    {
        //
    }

    /**
     * Show the form for editing the specified visitor.
     *
     * @param  Visitor $visitor
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(Visitor $visitor)
    {
        return view('dashboard.visitors.edit', compact('visitor'));
    }

    /**
     * Update the specified visitor in storage.
     *
     * @param VisitorUpdateRequest $request
     * @param Visitor $visitor
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(VisitorUpdateRequest $request, Visitor $visitor)
    {
        # check if repository not update visitor return alert.
        if( !$this->visitorsRepository->update($visitor, $request->except('_token', '_method')) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        return redirect()->route('visitors.index', $visitor->id)->with('success', __('system.updated'));
    }

    /**
     * Remove the specified visitor from storage.
     *
     * @param Visitor $visitor
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Visitor $visitor)
    {
        # check if repository not delete visitor return alert.
        if( !$this->visitorsRepository->delete($visitor) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        return redirect()->route('visitors.index')->with('success', __('system.deleted'));
    }

    /**
     * Update specification visitor password by id.
     *
     * @param UpdateUserPasswordRequest $request
     * @param Visitor $visitor
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updatePassword(UpdateUserPasswordRequest $request, Visitor $visitor)
    {
        # check if repository not update user password return alert.
        if( !$this->visitorsRepository->update( $visitor, $request->only('password') ) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        return redirect()->route('visitors.edit', $visitor->id)->with('success', __('system.updated'));
    }
}
