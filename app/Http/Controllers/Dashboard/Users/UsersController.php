<?php

namespace App\Http\Controllers\Dashboard\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\CreateUserRequest;
use App\Http\Requests\Users\UpdateUserPasswordRequest;
use App\Http\Requests\Users\UpdateUserRequest;
use App\Models\User;
use App\Repositories\Users\UsersRepository;

class UsersController extends Controller
{
    private $usersRepository;

    /**
     * UsersRepository constructor.
     *
     * @param UsersRepository $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    /**
     * Display a listing of the users.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        # Repository to get all users.
        $users = $this->usersRepository->get();

        return view('dashboard.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new user.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dashboard.users.create');
    }

    /**
     * Store a newly created user in storage.
     *
     * @param CreateUserRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateUserRequest $request)
    {
        # check if repository not create user return alert.
        if( !$this->usersRepository->create( $request->except('_token', 'after', 'password_confirmation') ) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        # check if submit on btn create & new
        if ($request->get('after') == 'new'){
            return redirect()->route('users.create')->with('success', __('system.created'));
        }

        return redirect()->route('users.index')->with('success', __('system.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show(int $id)
    {
        //
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param  User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(User $user)
    {
        return view('dashboard.users.edit', compact('user'));
    }

    /**
     * Update the specified user in storage.
     *
     * @param UpdateUserRequest $request
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        # check if repository not update user return alert.
        if( !$this->usersRepository->update($user, $request->except('_token', '_method')) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        return redirect()->route('users.index', $user->id)->with('success', __('system.updated'));
    }

    /**
     * Remove the specified user from storage.
     *
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(User $user)
    {
        # check if repository not delete user return alert.
        if( !$this->usersRepository->delete($user) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        return redirect()->route('users.index')->with('success', __('system.deleted'));
    }

    /**
     * Update specification user password by id.
     *
     * @param UpdateUserPasswordRequest $request
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updatePassword(UpdateUserPasswordRequest $request, User $user)
    {
        # check if repository not update user password return alert.
        if( !$this->usersRepository->update( $user, $request->only('password') ) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        return redirect()->route('users.edit', $user->id)->with('success', __('system.updated'));
    }
}
