<?php

namespace App\Http\Controllers\Dashboard\Articles;

use App\Http\Controllers\Controller;
use App\Http\Requests\Articles\ArticleStoreRequest;
use App\Http\Requests\Articles\ArticleUpdateRequest;
use App\Models\Article;
use App\Repositories\Articles\ArticlesRepositories;
use App\Repositories\Categories\CategoriesRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

class ArticlesController extends Controller
{
    protected $model;
    protected $categoriesRepository;

    public function __construct(ArticlesRepositories $articlesRepositories, CategoriesRepository $categoriesRepository)
    {
        $this->model = $articlesRepositories;
        $this->categoriesRepository = $categoriesRepository;
    }

    /**
     * Display a listing of the articles.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        # List articles.
        $articles = $this->model->get();

        return view('dashboard.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new article.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        # Repository to list categories.
        $categories = $this->categoriesRepository->list();

        return view('dashboard.articles.create', compact('categories'));
    }

    /**
     * Store a newly created article in storage.
     *
     * @param ArticleStoreRequest $request
     * @return Application|RedirectResponse|Redirector
     */
    public function store(ArticleStoreRequest $request)
    {
        # check if repository not create article return alert.
        if( !$this->model->create( array_merge($request->except('_token', 'after'), ['user_id' => auth()->user()->id]) ) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        # check if submit on btn create & new
        if ($request->get('after') == 'new'){
            return redirect()->route('articles.create')->with('success', __('system.created'));
        }

        return redirect()->route('articles.index')->with('success', __('system.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified article.
     *
     * @param Article $article
     * @return Application|Factory|View
     */
    public function edit(Article $article)
    {
        # Repository to list categories.
        $categories = $this->categoriesRepository->list();

        return view('dashboard.articles.edit', compact('article', 'categories'));
    }

    /**
     * Update the specified article in storage.
     *
     * @param Article $article
     * @param Request $request
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function update(ArticleUpdateRequest $request, Article $article)
    {
        # check if repository not update article return alert.
        if( !$this->model->update($article, $request->except('_token', '_method')) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        return redirect()->route('articles.index', $article->id)->with('success', __('system.updated'));
    }

    /**
     * Remove the specified article from storage.
     *
     * @param Article $article
     * @return Application|RedirectResponse|Redirector
     */
    public function destroy(Article $article)
    {
        # check if repository not delete article return alert.
        if( !$this->model->delete($article) )
            return redirect()->back()->with('warning', __('messages.field_process'));

        return redirect()->route('articles.index')->with('success', __('system.deleted'));
    }
}
