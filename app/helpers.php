<?php

use Illuminate\Support\Facades\Storage;

/**
 *  Remove file.
 *
 * @param string $path
 * @return bool
 */
function removeFile(string $path): bool
{
    # check if file exists remove.
    if(CheckIfFileExists($path))
        unlink(public_path() . '/' . $path);

    return true;
}

/**
 * Check file exists.
 *
 * @param string $path
 * @return bool
 */
function CheckIfFileExists(string $path): bool
{
    return file_exists( public_path($path)) ? true : false;
}

/**
 * Push array inside another array.
 *
 * @param array $array1
 * @param array $array2
 * @return array
 */
function push_array_inside_another_array(array $array1, array $array2): array
{
    foreach ($array1 as $key => $data){
        $array2[$key] = $data;
    }

    return $array2;
}

/**
 * Save setting image.
 *
 * @param $file
 * @param $name
 * @return string
 */
function saveSettingImage($file, $name): string
{
    # get file name.
    $fileName = $name . '.' . $file->extension();

    # save image.
    Storage::disk('local')->putFileAs('public/settings', $file, $fileName);

    return 'storage/settings/'. $fileName;
}
