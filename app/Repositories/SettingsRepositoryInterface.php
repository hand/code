<?php


namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Collection;

interface SettingsRepositoryInterface
{
    /**
     * Get model.
     *
     * @return mixed
     */
    public function get();

    /**
     * List model data.
     *
     * @return array
     */
    public function list(): array;

    /**
     * Save model data.
     *
     * @param array $data
     * @return Model
     */
    public function store(array $data): Model;

    /**
     * Delete model data
     *
     * @return bool
     */
    public function destroy();
}
