<?php

namespace App\Repositories\Categories;

use App\Enums\GeneralEnums;
use App\Models\Category;
use App\Repositories\EloquentRepositoryInterface;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Collection;

class CategoriesRepository extends Repository implements EloquentRepositoryInterface
{
    /**
     * CategoriesRepository constructor.
     *
     * @param Category $model
     */
    public function __construct(Category $model)
    {
        parent::__construct($model);
    }

    /**
     * List model.
     *
     * @return mixed
     */
    public function list()
    {
        return $this->model->withCount('articles')->get();
    }

    /**
     * Get model.
     *
     * @param int $perPage
     *
     * @return Collection
     */
    public function get(int $perPage = GeneralEnums::perPage)
    {
        return $this->model->orderBy('id', 'desc')->paginate($perPage);
    }

    /**
     * Get specification model by key.
     *
     * @param int $key
     *
     * @return Model
     */
    public function find(int $key): Model
    {
        return $this->model->where('id', $key)->first();
    }

    /**
     * Create new model.
     *
     * @param array $data
     *
     * @return bool
     */
    public function create(array $data): bool
    {
        try {

            $this->model->create($data);

        } catch (\Exception $e){

            return false;
        }

        return true;
    }

    /**
     * Update model.
     *
     * @param Model $model
     * @param array $payload
     *
     * @return bool
     */
    public function update(Model $model, array $payload): bool
    {
        try {

            # update model.
            $model->update($payload);

        } catch (\Exception $e){

            return false;

        }

        return true;
    }

    /**
     * Delete model.
     *
     * @param Model $model
     *
     * @return bool
     */
    public function delete(Model $model): bool
    {
        try {

            $model->delete();

        }catch (\Exception $e){

            return false;

        }

        return true;
    }
}
