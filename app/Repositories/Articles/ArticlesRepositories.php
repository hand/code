<?php


namespace App\Repositories\Articles;


use App\Enums\GeneralEnums;
use App\Models\Article;
use App\Repositories\EloquentRepositoryInterface;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Collection;

class ArticlesRepositories extends Repository implements EloquentRepositoryInterface
{
    /**
     * ArticlesRepository constructor.
     *
     * @param Article $model
     */
    public function __construct(Article $model)
    {
        parent::__construct($model);
    }

    /**
     * List model.
     *
     * @param int|null $category_id
     *
     * @param int $perPage
     */
    public function list(array $searchQuery = null, $perPage = GeneralEnums::perPage)
    {
        $query = $this->model->query()->where('status', 2);

        if( isset($searchQuery['category']) && $searchQuery['category'])
            $query->where('category_id', $searchQuery['category']);

        return $query->orderBy('id', 'desc')
            ->with('media')
            ->paginate($perPage);
    }

    /**
     * Get model.
     *
     * @param int $perPage
     * @return Collection
     */
    public function get(int $perPage = GeneralEnums::perPage)
    {
        return $this->model->orderBy('id', 'desc')->with('media', 'category')->paginate($perPage);
    }

    /**
     * List model.
     *
     * @return mixed
     */
    public function getN($count = 7)
    {
        return $this->model->where('status', 2)
            ->orderBy('id', 'desc')
            ->with('media')
            ->take($count)
            ->get();
    }

    /**
     * Get specification model by key.
     *
     * @param int $key
     * @return Model
     */
    public function find(int $key): Model
    {
        return $this->model->where('id', $key)->first();
    }

    /**
     * Create new model.
     *
     * @param array $data
     * @return bool
     */
    public function create(array $data): bool
    {
        try {

            $slider = $this->model->create($data);

            if (isset($data['article_image'])) {

                $slider->addMedia($data['article_image'])->toMediaCollection('article_image');

            }

        } catch (\Exception $e){

            return false;
        }

        return true;
    }

    /**
     * Update model.
     *
     * @param Model $model
     * @param array $payload
     * @return bool
     */
    public function update(Model $model, array $payload): bool
    {
        try {

            # update model.
            $model->update($payload);

            if (isset($payload['article_image'])) {

                $model->clearMediaCollection('article_image');

                $model->addMedia($payload['article_image'])->toMediaCollection('article_image');
            }

        } catch (\Exception $e){

            return false;

        }

        return true;
    }

    /**
     * Delete model.
     *
     * @param Model $model
     * @return bool
     */
    public function delete(Model $model): bool
    {
        try {

            $model->clearMediaCollection('article_image');

            $model->delete();

        }catch (\Exception $e){

            return false;

        }

        return true;
    }
}
