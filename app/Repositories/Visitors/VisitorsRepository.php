<?php

namespace App\Repositories\Visitors;

use App\Enums\GeneralEnums;
use App\Models\Visitor;
use App\Repositories\EloquentRepositoryInterface;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Collection;

class VisitorsRepository extends Repository implements EloquentRepositoryInterface
{
    /**
     * VisitorsRepository constructor.
     *
     * @param Visitor $model
     */
    public function __construct(Visitor $model)
    {
        parent::__construct($model);
    }

    /**
     * List model.
     *
     * @return mixed
     */
    public function list()
    {
        return $this->model->get();
    }

    /**
     * Get model.
     *
     * @param int $perPage
     *
     * @return Collection
     */
    public function get(int $perPage = GeneralEnums::perPage)
    {
        return $this->model->paginate($perPage);
    }

    /**
     * Get specification model by key.
     *
     * @param int $key
     * @return Model
     */
    public function find(int $key): Model
    {
        return $this->model->where('id', $key)->first();
    }

    /**
     * Create new model.
     *
     * @param array $data
     * @return bool
     */
    public function create(array $data): bool
    {
        try {

            $this->model->create($data);

        } catch (\Exception $e) {

            return false;
        }

        return true;
    }

    /**
     * Update model.
     *
     * @param Model $model
     * @param array $data
     * @return bool
     */
    public function update(Model $model, array $data): bool
    {
        try {

            $model->update($data);

        }catch (\Exception $e) {

            return false;
        }

        return true;
    }

    /**
     * Delete model.
     *
     * @param Model $model
     *
     * @return bool
     */
    public function delete(Model $model): bool
    {
        try {

            $model->delete();

        }catch (\Exception $e){

            return false;
        }

        return true;
    }
}
