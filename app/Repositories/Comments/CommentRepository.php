<?php

namespace App\Repositories\Comments;

use App\Enums\GeneralEnums;
use App\Models\Comment;
use App\Repositories\EloquentRepositoryInterface;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Collection;

class CommentRepository extends Repository
{
    /**
     * Estimates Repositories constructor.
     *
     * @param Comment $model
     */
    public function __construct(Comment $model)
    {
        parent::__construct($model);
    }

    /**
     * List model.
     *
     * @return mixed
     */
    public function list()
    {
        return $this->model->get();
    }

    /**
     * Create new model.
     *
     * @param array $data
     *
     * @return bool
     */
    public function create(array $data): bool
    {
        try {

            $this->model->create($data);

        } catch (\Exception $e){

            return false;
        }

        return true;
    }
}
