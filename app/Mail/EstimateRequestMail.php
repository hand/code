<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EstimateRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $fromEmail;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fromEmail, $data)
    {
        $this->data = $data;
        $this->fromEmail = $fromEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->fromEmail , 'Silvermanfence')
            ->view('mail.estimateRequestMail', ['data' => $this->data]);
    }
}
