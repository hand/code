<?php


namespace App\Enums;


class GeneralEnums
{
    # for paginate page count.
    const perPage = 10;

    # image mimes type.
    const mimesType = 'png,jpg,jpeg';

    # default images.
    const  DEFAULT_IMAGE_ADMIN_PLACEHOLDER = '/assets/backend/custom/images/person.jpg';
    const  DEFAULT_IMAGE_PLACEHOLDER = '/assets/admin/custom/images/placeholder.jpg';

    # default currency.
    const Currency = 'USD';
}
