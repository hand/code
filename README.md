
## Getting started

Launch the starter project:

- composer install

Next you need to make a copy of the .env.example file and rename it to .env inside your project root.

Run the following command to generate your app key:

- php artisan key:generate

Run the following command to generate new admin:

- php artisan db:seed

Run the following command to create database:

- php artisan migrate

Then start your server:

- php artisan serve
